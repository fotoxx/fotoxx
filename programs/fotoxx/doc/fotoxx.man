.TH FOTOXX 1 2020-01-01 "Linux" "Fotoxx man page"

Fotoxx - photo/image editor and collection manager

DESCRIPTION
   Organize and manage a large image collection. Edit and optimize photos and 
   other images, edit metadata, search images, and perform batch operations. 
   Fotoxx is a GTK GUI application. The included user manual explains Fotoxx
   operation in great detail. The following is a summary of capabilities.

CAPABILITIES 
   • Organize and manage a very large photo/image collection.
   • Thumbnail browser/navigator with adjustable thumb size and list view.
   • Click thumbnail for full-size view, image zoom in/out and pan/scroll.
   • RAW file conversion, single or batch, save with 8 or 16 bits/color.
   • Edit RAW files directly using RawTherapee or the Fotoxx edit functions.
   • Large set of functions to edit, repair, enhance, and transform images. 
   • Internal processing in 24 bits per color (float), output in 8 or 16 bits.
   • Edited files have a version number, originals are retained by default.
   • Fast edit visual feedback using the full image or selected zoom-in area.
   • Undo/Redo edits, go back and forth to compare original and edited versions.
   • Conventional edit functions include: brightness, contrast, color balance, 
     saturation, trim/crop, rotate, upright, resize, sharpen, denoise, blur, 
     paint, draw text/line/arrow/box, red eyes, smart erase (remove spoilers), 
     fix perspective, warp/unwarp, HDR, panorama, stack/edit, photo montage ...
   • Advanced edit functions to bring out details and add flair to an image:
     Edit brightness distribution, Global/zonal Retinex, Gradients ...
   • A large set of creative special effects and arty transforms is available:
     sketch, cartoon, drawing, emboss, tiles, dither, painting, mosaic, sphere, 
     tiny planet, Escher spiral, custom convolution kernels ...
   • Most edit functions can be 'painted' locally and gradually with the mouse.
   • Select image objects or areas to edit separately from the background:
     outline by hand, follow feature edges, 'flood' into matching colors ...
   • Special selection tool for complex image features (e.g. hair, foliage).
   • Copy areas within and across images by mouse painting and blending.
   • Create or maintain transparent image areas while editing. 
   • Mashup: arrange images and text in an arbitrary layout using the mouse.
   • Custom scripts: record a series of edits and use as a new edit function.
   • Custom favorites menu: select and arrange icons and text in popup window.
   • Plugins: use other edit apps (e.g. Gimp) as Fotoxx plug-in edit function.
   • Batch tools for renaming, resizing, converting, processing RAW files,
     adding/revising/reporting metadata, and executing custom edit scripts.
   • Metadata edit and report (tags, dates, captions, geotags ... any metadata).
   • Search images using any metadata and folder/file names or partial names:
     dates, tags, locations, ratings, captions, comments, exposure data ... 
   • Show a table of image locations and date groups, click for image gallery.
   • Show an image calendar, click on year or month for a gallery of images.
   • Click markers on a scalable internet map for gallery of images at location.
   • Use locally stored maps: world, continents, nations, cities, custom maps.
   • View 360 degree panorama image (Google Street View format).
   • Show video files (first frame) in thumbnails, view full size, play video.
   • Show animated GIF files (first frame) in thumbnails, play animation.
   • Bookmarks: assign names to file locations, list names, goto name (gallery).
   • Create albums linking selected images. Arrange sequence by drag and drop.
   • Slide show: show album images with animated transitions and pan/zoom.
   • Print an image at any scale. Printer color calibration tool is available.
   • Custom keyboard shortcuts can be assigned to most functions.
   • Comprehensive user guide and context help popups via F1 key.

SEE ALSO
   The User Guide is available from the menu Help > User Guide 
   The home page for Fotoxx is at https://kornelix.net

AUTHORS
   Written by Mike Cornelison <mkornelix@gmail.com>

