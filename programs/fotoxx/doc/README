Installation of fotoxx from source tarball
 
 Building fotoxx requires the following packages:  
    g++ or clang++           C++ compiler and linker 
    libgtk-3-dev             GTK3 graphics library (GUI base)
    libtiff-*-dev            tiff image files
    libpng-*-dev             png  image files
    libjpeg-*-dev            jpeg image files
    liblcms2-*-dev           color spaces (sRGB/Adobe-RGB)
    libchamplain-gtk-*-dev   geomapping library
    libclutter-gtk-*-dev     graphics library

 At run time the following packages are also needed:
    dcraw               RAW file conversion program
    exiftool            read/write metadata (EXIF etc.) 
    binutils            GNU binary utilities
    web browser         firefox or chromium 
 
 The above are recent Debian names. Package naming is chaotic, 
 so you may have to to hunt the names for other Linux flavors.
 
 Build and install fotoxx as follows:
    1. Download the tar file (fotoxx-N.N.tar.gz)
    2. Open a terminal window 
    3. $ cd Downloads                    # go to download file
    4. $ tar -xzf fotoxx-N.N.tar.gz      # unpack to ./fotoxx 
    5. $ cd fotoxx                       # go there 
    6. $ make                            # build program 
    7. $ sudo make install               # install program
   
 Missing dependencies will cause error messages in step 6. 
 Install these from your repository and repeat step 6. 
  
 Step 7 moves all files to the following locations:
    /usr/bin/fotoxx             binary executable 
    /usr/share/fotoxx/          icons, translations ...
    /usr/share/doc/fotoxx/      README, man page ...
 
 Fotoxx is easy to use but unconventional, so please review
 the user guide (menu Help > User Guide) before trying fotoxx.
 
 The optional fotoxx-maps package may be installed afterwards.
 This is only useful if you have a poor internet connection.
 
 FOR PACKAGE BUILDERS:
 If $PREFIX is defined, files go there instead of /usr. 
 If $DESTDIR is also defined, files go to $DESTDIR$PREFIX. 
 Please, DO NOT make a separate package for the all-arch parts.
 
