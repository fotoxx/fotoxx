#! /bin/bash
#
#  Update translations for the given program (merge new strings).
#
#  usage:  $ translate appname lc
#
#  "lc" is language [ _region ] code, e.g. "de" or "de_AT"
#  "lc" may be "all" to process all .po files
#
#  procedure:
#     run this script to get new strings added to appname-lc.po file
#     edit appname-lc.po file: add missing translations
#     run this script again to check for errors

appname=$1
lang=$2

if [ "$appname" = "" -o "$lang" = "" ]; then                                     #  verify $ translation appname lc_RC
   echo  usage: translate appname lc [ _RC ]
   exit 1
fi

directory=$programs/$appname
cd  $directory > /dev/null 2>&1
if [ $? -ne 0 ]; then
   echo $directory does not exist
   exit 1
fi

if [ $lang = "all" ]; then                                                       #  do all languages
   langlist=$(find locales/*.po)
   for lang in  $langlist
   do                                                                            #  translate-lc.po
      lcpo=${lang#locales/translate-}                                            #  lc.po
      lc=${lcpo%.po}                                                             #  lc
      /home2/mico/programs/bash/translate $appname $lc
      if [ $? -ne 0 ]; then exit 1; fi
   done
   echo ""
   exit 0
fi

pofile=$directory/locales/translate-$lang.po                                     #  locales/translate-xx.po

mkdir -p locales                                                                 #  create locales directory if needed

if [ ! -f $pofile ]; then                                                        #  if no prior *.po files, create them
   echo initializing language $lang
   msginit -l $lang  -o $pofile  -i translate.pot
fi

dos2unix $pofile > /dev/null 2>&1                                                #  replace CR/LF (Microsoft) with LF (Unix)

echo  translate $appname                                                         #  get text strings from *.cc files
xgettext -s --keyword=E2X --from-code=UTF-8 -o translate.pot  *.cc *.h
if [ ! -f translate.pot ]; then
   echo xgettext failed
   exit 1
fi

echo "merging new strings for language $lang"                                    #  add new strings to prior *.po files
msgmerge -U -N -F $pofile  translate.pot
msgfmt -v  --check-format  -o /dev/null $pofile                                  #  check format codes are correct
if [ $? -ne 0 ]; then exit 1; fi

rm -f locales/*.po~                                                              #  purge temp and *.pot files
rm -f *.pot

exename=${appname%-test}
exename=${exename%-*}

$programs/utils/missing_po $exename $lang                                        #  dump missing translations

echo ""
exit 0


