#! /bin/bash
#
#  build .deb package
#
#  $ packdeb appname-NN.N                                                        # appname-NN.N = directory name
#  $ packdeb all
#

appdir=$1                                                                        # appname-NN.N [ -test ]
if [ -z "$appdir" ]; then
   echo "usage: packdeb appname-NN.N"
   exit 1
fi

sudo echo -n ""                                                                  # get privileges

if [ $appdir = "all" ]; then                                                     # build all packages
   applist=""
   while read record; do
      applist+="$record "
   done < "$programs/bash/proglist"

   for appdir in $applist
   do 
      $programs/bash/packdeb $appdir
   done
   echo ""
   exit 0
fi

cd $programs/$appdir 2>/dev/null                                                 # .../programs/appname-NN.N
if [ $? -ne 0 ]; then
   echo $programs/$appdir does not exist
   exit 1
fi

if [ ! -f  $programs/$appdir/debian-control ]; then
   echo $programs/$appdir not a debian package
   echo ""
   exit 1
fi

appname=${appdir%-test}                                                          # remove -test and -NN.N
appname=${appname%-*}                                                            # appname
appvers=${appdir#$appname-}                                                      # NN.N

echo $appvers | grep -c "test" >/dev/null                                        # if version NN.N-test
if [ $? -eq 0 ]; then                                                            #   make it NN.N-test-yyyy-mm-dd
   date=$(date -I)
   appvers=$appvers-$date
fi

tarfile=$appname-$appvers.tar.gz                                                 # appname-NN.N [ -test-yyyy-mm-dd ] .tar.gz
package=$appname-$appvers-x86_64.deb                                             # appname-NN.N [ -test-yyyy-mm-dd ] -x86_64.deb

bash $programs/bash/binstall $appdir                                             # build binary, install all files
if [ $? -ne 0 ]; then exit 1; fi

sudo rm -f -R debian
mkdir debian                                                                     # create debian directories
mkdir -p debian/DEBIAN
mkdir -p packs                                                                   # package output location

if [ -f $appname ]; then
   mkdir -p debian/usr/bin
   cp -f $appname debian/usr/bin                                                 # copy application files
fi

if [ -d data ]; then
   mkdir -p debian/usr/share/$appname/data
   cp -f -R data/* debian/usr/share/$appname/data
fi

if [ -d html ]; then
   mkdir -p debian/usr/share/$appname/html
   cp -f -R html/* debian/usr/share/$appname/html
fi

if [ -d images ]; then
   mkdir -p debian/usr/share/$appname/images
   cp -f -R images/* debian/usr/share/$appname/images
fi

if [ -d locales ]; then
   mkdir -p debian/usr/share/$appname/locales
   cp -f -R locales/* debian/usr/share/$appname/locales
   gzip debian/usr/share/$appname/locales/*
fi

if [ -d doc ]; then
   mkdir -p debian/usr/share/doc/$appname
   cp -f -R doc/* debian/usr/share/doc/$appname
fi

if [ -f doc/changelog ]; then
   gzip -f -9 debian/usr/share/doc/$appname/changelog                            # compress changelog
fi   

if [ -f doc/$appname.man ]; then                                                 # create man page
   mkdir -p debian/usr/share/man/man1
   cp -f doc/$appname.man $appname.1
   gzip -f -9 $appname.1
   cp $appname.1.gz debian/usr/share/man/man1
   rm $appname.1.gz
fi

if [ -f $appname.desktop ]; then                                                 # create desktop/menu file
   mkdir -p debian/usr/share/applications
   cp -f $appname.desktop debian/usr/share/applications/$appname.desktop
fi

if [ -f $appname.png ]; then                                                     # copy icon file
   mkdir -p debian/usr/share/$appname/icons
   cp -f $appname.png debian/usr/share/$appname/icons
fi

if [ -d appdata ]; then                                                          # copy appdata file
   mkdir -p debian/usr/share/appdata
   cp -f -R appdata/* debian/usr/share/appdata
fi

ls -s -1 /usr/bin/$appname > temp 2>/dev/null                                    # get installed size
ls -s -R -1 /usr/share/$appname | grep total >> temp
ls -s -R -1 /usr/share/doc/$appname | grep total >> temp
sed 's\/usr/bin/'$appname'\\g' temp > temp2
sed 's\total\\g' temp2 > temp
kbytes=0
exec 99< temp
while true; do
   read -u 99 record
   if [ $? -ne 0 ]; then break; fi
   kbytes=$(($kbytes+$record))
done
rm temp temp2

sed -i "/Installed-Size:/ c Installed-Size: $kbytes" debian-control              # installed size >> debian control file
sed -i "/Version:/ c Version: $appvers"  debian-control                          # version >> debian control file
cp -f debian-control debian/DEBIAN/control                                       # copy debian control file

sudo find debian -type d -exec chmod 0755 {} +                                   # directory permissions
sudo find debian -type f -exec chmod 0644 {} +                                   # file permissions
chmod 0755 debian/usr/bin/$appname 2>/dev/null                                   # executable permissions
sudo chown -f -R 0:0 debian                                                      # make root own all files

dpkg-deb --build debian packs/$package > errmess 2>&1                            # build .deb package
if [ $? -ne 0 ]; then 
   echo dpkg status: $(cat errmess)
   exit 1
fi

lintian -F packs/$package > errmess 2>&1                                         # lintian package checks
if [ $? -ne 0 ]; then
   echo lintian status: $(cat errmess) 
   exit 1
fi

sudo dpkg -i packs/$package > errmess 2>&1                                       # install package
if [ $? -ne 0 ]; then
   echo dpkg status: $(cat errmess) 
   exit 1
fi

echo $package
echo packdeb status $?
echo ""

sudo rm -f -R debian                                                             # clean up
rm -f errmess
exit 0


